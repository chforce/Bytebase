## Help file naming rules

Let's name our help files based on routes/domains, and all files should start with `help.` to indicate that it is a help doc. For example, `help.environment.md` is the help file that stands for the whole `/environment` page. If you want to create a more detailed help file to explain *Approval Policy* under `\environment`, you can name the file to `help.environment.approval-policy.md`.

If the objective of the explanation doesn't belong to any specific route/domain, we can name it with `help.global...`.
